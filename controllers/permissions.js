const perms = require('../config/permissions.json');
const config = require('../config/config.json');

class PermissionsBot {
    
    constructor(bot) {
        this._bot = bot;
        this._perms = perms;
        this._old = "";
        this._last = new Array();
    }

    sendMessage(message) {
        if (this._writeChan)
            this._writeChan.send(message);
    }

    checkKick(from) {
        const arr = this._last.filter((elem) => {
            return elem === from.id
        })
        if (arr.length >= config.kickLimit) {
            from.kick("Oh yea nigga");
            this.sendMessage(`${from.member.displayName}. Rip spammer`)
            this._last = [];
            return undefined;
        }
        return from;
    }

    addKick(from) {
        if (this._last.length === config.kickBuffer) {
            this._last.splice(0, 1);
        }
        this._last.push(from.id);
        return this.checkKick(from);
    }

    checkPerm(from, choice) {
        const oldChannel = this._bot.channels.resolve(from.channelID);
        const newChannel = this._bot.channels.resolve(choice.channelID);
        this._writeChan = this._bot.channels.resolve(config.writeChannelId);

        if (this._old === from.id) {
            this._old = "";
            return;
        }
        if ((oldChannel && newChannel) && this._perms[oldChannel.name]) {
            if (this._perms[oldChannel.name].indexOf(newChannel.name) === -1) {
                this._old = from.id;
                from = this.addKick(from);
                if (from) {
                    from.setChannel(oldChannel);
                }
            }
        } else if (!oldChannel) {
            if (newChannel.name !== this._perms.entry) {
                this.sendMessage(`${from.member.displayName}, On t'as jamais appris à sonner avant d'entrer ?`)
                from.kick('Bye bye');
            }
        }
    }
}

module.exports = PermissionsBot;