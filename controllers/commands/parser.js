const RouletteCommand = require('./fun/rouletteCommand');
const RandomGameCommand = require('./fun/randomGameCommand');
const CulSecCommand = require('./fun/culSecCommand');
const HelpCommand = require('./fun/helpCommand');

const StopCommand = require('./admin/stopCommand');
const StartCommand = require('./admin/startCommand');
const StartRandomVoiceCommand = require('./admin/startRandomVoice');
const StopRandomVoiceCommand = require('./admin/stopRandomVoice');
const config = require('../../config/config.json');

class Parser {

    constructor(bot, botInstance) {
        this._bot = bot;
        this._commands = {};
        this._botInstance = botInstance;
        this._commands['&roulette'] = new RouletteCommand(bot);
        this._commands['&stopwatch'] = new StopCommand(bot);
        this._commands['&startwatch'] = new StartCommand(bot);
        this._commands['&startrandomvoice'] = new StartRandomVoiceCommand(bot);
        this._commands['&stoprandomvoice'] = new StopRandomVoiceCommand(bot);
        this._commands['&randomgame'] = new RandomGameCommand(bot);
        this._commands['&cusecmortel'] = new CulSecCommand(bot);
        this._commands['&help'] = new HelpCommand(bot);
    }

    parseCommand(content, user) {
        const args = content.split(' ');
        try {
            this._commands[args[0]].command(args, user, this._botInstance);
        } catch {
            this._writeChan = this._bot.channels.resolve(config.writeChannelId);
            this._writeChan.send(`Commande inconnue, voir documentation (&help)`);
        }
    }
}

module.exports = Parser;