const config = require('../../../config/config.json');

class StopCommand {
    
    constructor(bot) {
        this._bot = bot;
    }

    isOwner(id) {
        let bool = false;
        config.botOwners.forEach((userId) => {
            if (userId === id) {
                bool = true;
            }
        })
        return bool;
    }

    command(args, user, botInstance) {
        this._writeChan = this._bot.channels.resolve(config.writeChannelId);
            if (this.isOwner(user.id)) {
                botInstance._watch = false;
                this._writeChan.send(`Watcher arrêté`);
            } else {
                this._writeChan.send(`Permissions insuffisantes`)
            }
    }
}

module.exports = StopCommand;