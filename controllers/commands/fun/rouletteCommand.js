const config = require('../../../config/config.json');

class RouletteCommand {
    
    constructor(bot) {
        this._bot = bot;
        this._choices = [];
        this._numbers = [];
    
        this.generateChoices();
    }
    
    generateChoices() {
        for (let i = 0; i < 51; i++) {
            this._choices.push('Bois');
        }
        for (let i = 0; i < 51; i++) {
            this._choices.push('Donnes');
        }
        for (let i = 0; i < 25; i++) {
            this._numbers.push('1');
        }
        for (let i = 0; i < 25; i++) {
            this._numbers.push('2');
        }
        for (let i = 0; i < 20; i++) {
            this._numbers.push('3');
        }
        for (let i = 0; i < 15; i++) {
            this._numbers.push('4');
        }
        for (let i = 0; i < 10; i++) {
            this._numbers.push('5');
        }
        for (let i = 0; i < 5; i++) {
            this._numbers.push('Cul-Sec');
        }
        this._choices = this.shuffle(this._choices);
        this._numbers = this.shuffle(this._numbers);
    }

    shuffle(arr) {
        let currentIndex = arr.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
          temporaryValue = arr[currentIndex];
          arr[currentIndex] = arr[randomIndex];
          arr[randomIndex] = temporaryValue;
        }
        return arr;
      }

    command() {
        const choice = this._choices[Math.floor(Math.random() * this._choices.length)];
        const number = this._numbers[Math.floor(Math.random() * this._numbers.length)];
        
        this._writeChan = this._bot.channels.resolve(config.writeChannelId);
        this._writeChan.send(`Tu ${choice} ${number}`);
    }
}

module.exports = RouletteCommand;