const config = require('../../../config/config.json');

class HelpCommand {
    
    constructor(bot) {
        this._bot = bot;
    }

    command(args, user) {
        this._writeChan = this._bot.channels.resolve(config.writeChannelId);
        this._writeChan.send(`==== HELP ====
==============
Admin:
&startwatch: Démarrer le système de permission (admin)
&stopwatch: Arrêter le système de permission (admin)
&startrandomvoice: Démarrer le mode chien (admin)
&stoprandomvoice: Arrêter le mode chien (admin)
==============
Fun:
&roulette: Jeu de la roulette, tu donnes ou tu bois
&cusecmortel NB : Détermine NB joueurs dans le channel vocal du lanceur pour un cul sec mortel !
&randomgame: Détermine un jeu au hasard dans la configuration`);
    }
}

module.exports = HelpCommand;