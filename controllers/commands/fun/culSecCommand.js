const config = require('../../../config/config.json');

class CulSecCommand {
    
    constructor(bot) {
        this._bot = bot;
        this._games = config.games;
        this._games = this.shuffle(this._games);
    }

    shuffle(arr) {
        let currentIndex = arr.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
          temporaryValue = arr[currentIndex];
          arr[currentIndex] = arr[randomIndex];
          arr[randomIndex] = temporaryValue;
        }
        return arr;
      }

    randomUser(args, user) {
      let users = user.voice.channel.members;
      let members = [];
      users.forEach((user) => {
        members.push(user.displayName);
      })
      for (let i = 0; i < parseInt(args[1]); i++) {
        members = this.shuffle(members);
      }
      
      const choices = [];
      for (let i = 0; i < parseInt(args[1]); i++) {
        choices.push(members[Math.floor(Math.random() * members.length)]);
      }

      return (choices);
    }

    command(args, user) {
        this._writeChan = this._bot.channels.resolve(config.writeChannelId);
        this._writeChan.send(`Chargement..`);
        const choices = this.randomUser(args, user);
        let str = "";
        choices.forEach((choice) => {
          str += choice + ', '
        })
        str += 'CUL SEC !'
        this._writeChan.send(str);
    }
}

module.exports = CulSecCommand;