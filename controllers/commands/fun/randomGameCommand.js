const config = require('../../../config/config.json');

class RandomGameCommand {
    
    constructor(bot) {
        this._bot = bot;
        this._games = config.games;
        this._games = this.shuffle(this._games);
    }

    shuffle(arr) {
        let currentIndex = arr.length, temporaryValue, randomIndex;
        while (0 !== currentIndex) {
          randomIndex = Math.floor(Math.random() * currentIndex);
          currentIndex -= 1;
          temporaryValue = arr[currentIndex];
          arr[currentIndex] = arr[randomIndex];
          arr[randomIndex] = temporaryValue;
        }
        return arr;
      }

    command() {
        const game = this._games[Math.floor(Math.random() * this._games.length)];
        
        this._writeChan = this._bot.channels.resolve(config.writeChannelId);
        this._writeChan.send(`Jeu décidé : ${game}`);
    }
}

module.exports = RandomGameCommand;