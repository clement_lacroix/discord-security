const Discord = require("discord.js")
const config = require('../config/config.json');
const Permissions = require('./permissions');
const Parser = require('./commands/parser');
const RandomVoice = require('./modules/randomVoice');

class Bot {
    constructor() {
        this._bot = new Discord.Client();
        this._permManager = new Permissions(this._bot);
        this._randomVoice = false;
        this._parser = new Parser(this._bot, this);
        this.modules = {};
        this.modules['randomVoice'] = new RandomVoice(this._bot);
        this._bot.login(config.botToken);
        this._watch = true;
        console.log('Launched');
        this.listen();
    }

    isAdmin(id) {
        let bool = false;
        config.exceptionsPermission.forEach((userId) => {
            if (userId === id)
                bool = true;
        })
        return bool;
    }

    listen() {
        this._bot.on('voiceStateUpdate', (oldState, newState) => {
            if (this._watch === true && newState)
                if (this.isAdmin(newState.id) === false) {
                    this._permManager.checkPerm(oldState, newState);
                }
        })

        this._bot.on('message', (msg) => {
            if (msg.content[0] === '&' && msg.channel.id === config.writeChannelId)
                this._parser.parseCommand(msg.content, msg.member);
        })
    }
    
    
}

module.exports = Bot;
