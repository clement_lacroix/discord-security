const config = require('../../config/config.json');

class randomVoice {
    constructor(bot) {
        this._bot = bot;
        this.channels = [];
        this.channel = [];
        this._sound = '';
        this.active = false;
    }

    run() {
        if (config.runVoiceRandom === true && this.active === true) {
            this.selectRandom();
            this.channel.join().then((connection) => {
                const dispatch = connection.play(`${config.soundPath}${this._sound}`);
                setTimeout(() => {
                    this.channel.leave();
                    return;
                }, 5500);
            }).catch(() => {
                console.log('err');
                return;
            });
        }
    }

    selectRandom() {
        this._bot.channels.cache.forEach((channel) => {
            if (channel.type === 'voice' && channel.joinable === true)
                this.channels.push(channel);
        })
        this.channel = this.channels[Math.floor(this.channels.length * Math.random())]
        this._sound = config.randomSounds[Math.floor(config.randomSounds.length * Math.random())];
    }
}

module.exports = randomVoice;